const problem3 = (inventory) => {
    if((Array.isArray(inventory)) && (inventory.length!==0)){
    // return inventory.sort((a,b) => {
    //     return a.car_model - b.car_model
    // })
   let arr = inventory.sort((a, b) => {
        let fa = a.car_model.toLowerCase(),
            fb = b.car_model.toLowerCase();
    
        if (fa < fb) {
            return -1;
        }
        if (fa > fb) {
            return 1;
        }
        return 0;
    });
    return arr
    }


    return []
}

module.exports = problem3;
