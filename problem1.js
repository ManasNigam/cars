const problem1 = function (inventory, id) {


    if (Number.isInteger(id) && Array.isArray(inventory) && inventory.length !== 0) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id === id) {
                return [inventory[i]]

            }
        }
    }

    return []
}



module.exports = problem1;
