const problem5 = (inventory,year) => {
    
    if ((Array.isArray(inventory)) && (inventory.length !== 0 && Number.isInteger(year))) {
        let answer = []
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].car_year < year) {
                answer.push(inventory[i])
            }
        }
        return answer.length
    }
    else {
        return []
    }
}

module.exports = problem5
